<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

require_once(dirname(__FILE__).'/includes/core.inc.php');

header('Location: '.$config['main-website']['url']);
