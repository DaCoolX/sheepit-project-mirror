FROM jejem/php:7.4-apache
MAINTAINER Laurent Clouet <laurent.clouet@nopnop.net>

ARG FULL_VERSION=dev
ENV VERSION=$FULL_VERSION

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install \
	coreutils \
	cron

RUN apt-get clean && \
	apt-get autoclean && \
	rm -rf /var/lib/apt/lists/*

RUN echo "memory_limit = 1G" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "post_max_size = 787M" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "upload_max_filesize = 787M" >> /usr/local/etc/php/conf.d/custom.ini && \
	echo "apc.enable_cli = 1" >> /usr/local/etc/php/conf.d/custom.ini

# create random file for speed test
RUN dd if=/dev/urandom of=/var/www/html/speedtest.zip bs=1M count=1

COPY --chown=www-data:www-data . /var/www/html
RUN rm -f /var/www/html/index.html
RUN ln -sf /etc/sheepit-mirror/config.local.inc.php /var/www/html/includes/config.local.inc.php

RUN runuser -s /bin/bash -c "cd /var/www/html/lib && composer install --no-dev --no-progress --no-suggest --optimize-autoloader" - www-data

COPY docker/etc/cron.d/sheepit-mirror /etc/cron.d/
RUN chmod 644 /etc/cron.d/sheepit-mirror
COPY docker/entrypoint.sh /
RUN chmod 755 /entrypoint.sh

VOLUME ["/etc/sheepit-mirror", "/tmp/projects", "/tmp/logs"]
