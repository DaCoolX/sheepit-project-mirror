<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

require_once(dirname(__FILE__).'/includes/core.inc.php');

if (isFromMainServer() == false) {
	Logger::error(__file__.':'.__line__.' request banned because remove_addr is \''.$_SERVER['REMOTE_ADDR'].'\'');
	header('HTTP/1.1 404 Not Found');
	die();
}

header('HTTP/1.1 200 OK');
echo getenv('VERSION');
